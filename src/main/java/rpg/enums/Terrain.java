package rpg.enums;

public enum Terrain
{
    PLAINS, RAIN_FOREST, DESERT, TUNDRA, MOUNTAINS, WATER
}
