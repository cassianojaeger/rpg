package rpg.enums;

public enum PotionSize
{
    SMALL, NORMAL, BIG, GIANT
}
