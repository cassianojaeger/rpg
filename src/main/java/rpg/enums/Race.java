package rpg.enums;

public enum Race
{
    DRAGON, HUMAN, ELF, DWARF;
}
