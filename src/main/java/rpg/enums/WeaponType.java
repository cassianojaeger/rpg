package rpg.enums;

public enum WeaponType
{
    AXE, SWORD, DUAL_SWORD, PISTOL, BOW, STAFF;
}
