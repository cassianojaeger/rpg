package rpg.enums;

import lombok.Getter;

@Getter
public enum Action
{
    ATTACK("Attack"), USE_POTION("Use Potion"), SURRENDER("Surrender"), DO_NOTHING("Do Nothing");

    private final String code;

    Action(String code)
    {
        this.code = code;
    }
}
