package rpg.enums;

public enum WeaponRank
{
    NORMAL, EPIC, LEGENDARY, ANCIENT;
}
