package rpg.items.weapons;

import lombok.Builder;
import lombok.ToString;
import rpg.enums.WeaponRank;
import rpg.enums.WeaponType;
import lombok.Getter;
import lombok.Setter;
import rpg.items.Item;
import rpg.items.potions.Potion;

@Getter
@Setter
@ToString(callSuper = true)
public class Weapon extends Item
{
    private WeaponRank rank;
    private WeaponType type;
    private Double damage;
    private Double durability;
    private Double defensePenetration;

    @Builder
    public Weapon(String name, WeaponRank rank, WeaponType type, Double damage, Double durability, Double defensePenetration)
    {
        super(name);
        this.rank = rank;
        this.type  = type;
        this.damage = damage;
        this.durability = durability;
        this.defensePenetration = defensePenetration;
    }
}
