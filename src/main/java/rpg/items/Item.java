package rpg.items;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Item
{
    private String name;
}
