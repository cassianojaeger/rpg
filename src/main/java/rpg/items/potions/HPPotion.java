package rpg.items.potions;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.PotionSize;

@Getter
@Setter
@ToString(callSuper = true)
public class HPPotion extends Potion
{
    private Double hpValue;

    @Builder
    public HPPotion(String name, PotionSize size, Double hpValue)
    {
        super(name, size);
        this.hpValue = hpValue;
    }
}
