package rpg.items.potions;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.PotionSize;

@Getter
@Setter
@ToString(callSuper = true)
public class MPPotions extends Potion
{
    private Double mpValue;

    @Builder
    public MPPotions(String name, PotionSize size, Double mpValue)
    {
        super(name, size);
        this.mpValue = mpValue;
    }
}
