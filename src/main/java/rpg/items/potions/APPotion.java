package rpg.items.potions;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.PotionSize;

@Getter
@Setter
@ToString(callSuper = true)
public class APPotion extends Potion
{
    private Double apValue;

    @Builder
    public APPotion(String name, PotionSize size, Double apValue)
    {
        super(name, size);
        this.apValue = apValue;
    }
}
