package rpg.items.potions;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.PotionSize;
import rpg.items.Item;

@Getter
@Setter
@ToString(callSuper = true)
public class Potion extends Item
{
    private PotionSize size;
    public Potion(String name, PotionSize size)
    {
        super(name);
        this.size = size;
    }
}
