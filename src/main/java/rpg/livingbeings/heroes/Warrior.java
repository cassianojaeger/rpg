package rpg.livingbeings.heroes;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.Race;
import rpg.items.Item;
import rpg.items.weapons.Weapon;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class Warrior extends Hero
{
    private Double ragePoints;

    @Builder
    Warrior(String name, int level, Race race, Double hitPoints, Weapon weapon, Double ragePoints, Double defense, List<Item> inventory)
    {
        super(name, level, hitPoints, race, weapon, defense, inventory);
        this.ragePoints = ragePoints;
    }
}
