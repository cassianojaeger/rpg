package rpg.livingbeings.heroes;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.Race;
import rpg.items.Item;
import rpg.items.weapons.Weapon;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class Mage extends Hero
{
    private Double manaPoints;

    @Builder
    Mage(String name, int level, Race race, Double hitPoints, Weapon weapon, Double manaPoints, Double defense, List<Item> inventory)
    {
        super(name, level, hitPoints, race, weapon, defense, inventory);
        this.manaPoints = manaPoints;
    }
}
