package rpg.livingbeings.heroes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.Race;
import rpg.items.Item;
import rpg.items.weapons.Weapon;
import rpg.livingbeings.Being;

import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
public class Hero extends Being
{
    private Race race;
    private Weapon weapon;
    private List<Item> inventory;

    public Hero(String name, int level, Double hitPoints, Race race, Weapon weapon, Double defense, List<Item> inventory)
    {
        super(name, level, hitPoints, defense);
        this.race = race;
        this.weapon = weapon;
        this.inventory = inventory;
    }
}
