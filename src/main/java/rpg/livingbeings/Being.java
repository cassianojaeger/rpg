package rpg.livingbeings;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Being
{
    private String name;
    private int level;
    private Double hitPoints;
    private Double defense;
}
