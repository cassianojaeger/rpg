package rpg.livingbeings.monsters;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import rpg.livingbeings.Being;

@Getter
@Setter
public class Monster extends Being
{
    private Double damage;

    @Builder
    public Monster(String name, int level, Double hitPoints, Double defense, Double damage)
    {
        super(name, level, hitPoints, defense);
        this.damage = damage;
    }
}
