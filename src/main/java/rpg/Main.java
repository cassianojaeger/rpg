package rpg;

import rpg.battles.Battle;
import rpg.battles.BattleResult;
import rpg.enums.Terrain;
import rpg.enums.Race;
import rpg.enums.WeaponRank;
import rpg.enums.WeaponType;
import rpg.livingbeings.heroes.Archer;
import rpg.livingbeings.heroes.Mage;
import rpg.livingbeings.heroes.Warrior;
import rpg.livingbeings.monsters.Monster;
import rpg.items.weapons.Weapon;

public class Main
{
    public static void main(String[] args)
    {
        Weapon staff = Weapon.builder().damage(15.0).durability(1.0).rank(WeaponRank.NORMAL).name("White Staff").type(WeaponType.STAFF).defensePenetration(0.5).build();
        Mage mage = Mage.builder().name("Gandalf").hitPoints(100.0).manaPoints(200.0).race(Race.HUMAN).weapon(staff).level(1).defense(8.0).build();

        Weapon bow = Weapon.builder().damage(20.0).durability(1.0).rank(WeaponRank.NORMAL).name("Normal Bow").type(WeaponType.BOW).defensePenetration(0.3).build();
        Archer archer = Archer.builder().name("Legolas").hitPoints(120.0).abilityPoints(150.0).race(Race.ELF).weapon(bow).level(1).defense(10.0).build();

        Weapon sword = Weapon.builder().damage(18.0).durability(1.0).rank(WeaponRank.NORMAL).name("Normal Sword").type(WeaponType.SWORD).defensePenetration(0.2).build();
        Warrior warrior = Warrior.builder().name("Aragorn").hitPoints(150.0).ragePoints(180.0).race(Race.HUMAN).weapon(sword).level(1).defense(20.0).build();

        Monster orc = Monster.builder().name("Orc").hitPoints(50.0).level(1).defense(3.0).damage(5.0).build();

        BattleResult result = Battle.builder().hero(warrior).monster(orc).terrain(Terrain.RAIN_FOREST).build().start();

        System.out.println(result.toString());
    }
}
