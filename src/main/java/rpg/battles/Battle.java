package rpg.battles;

import lombok.*;
import rpg.enums.Action;
import rpg.enums.Terrain;
import rpg.livingbeings.Being;
import rpg.livingbeings.heroes.Hero;
import rpg.livingbeings.monsters.Monster;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

@Getter
@Setter
@ToString
@Builder
public class Battle
{
    private Terrain terrain;
    private Monster monster;
    private Hero hero;
    final Scanner keyboard = new Scanner(System.in);
    final ActionContext actionContext = new ActionContext();
    final BattleResultProcessor battleResultProcessor = new BattleResultProcessor();

    private static String buildOptions(Action a)
    {
        return String.format("%n %s - %s", a.getCode(), a.getCode().substring(0, 1));
    }

    public BattleResult start()
    {
        ArrayList<Being> opponents = new ArrayList<>(Arrays.asList(hero, monster));
        ArrayList<RoundResult> rounds = new ArrayList<>();

        while (!battleIsOver())
        {
            RoundResult round = playRound(opponents);
            rounds.add(round);
            Collections.rotate(opponents, 1);
            System.out.println(round.getAttacker().getName() + " " + round.getActionPerformed().getCode() + " " + round.getDefender().getName());
            System.out.println("Damage dealt was: " + round.getDamageDealt());
            System.out.printf("%s life is: %s%n", round.getDefender().getName(), round.getDefender().getHitPoints());
            System.out.printf("%s life is: %s%n", round.getAttacker().getName(), round.getAttacker().getHitPoints());
        }

        return battleResultProcessor.processResults(rounds);
    }

    @SneakyThrows
    private RoundResult playRound(ArrayList<Being> opponents)
    {
        Being attacker = opponents.get(0);
        Being defender = opponents.get(1);
        Action action;

        if (attacker instanceof Hero)
        {
            String options = stream(Action.values()).map(Battle::buildOptions).collect(Collectors.joining());
            System.out.printf("%nIt is your turn! %s %nTake an action: %s %n", attacker.getName(), options);
            action = resolveAction(keyboard.nextLine());
        } else
        {
            System.out.printf("%nThe enemy %s is moving! Pay attention. %n", attacker.getName());
            Thread.sleep(2000);
            action = generateRandomAction();
        }

        return actionContext.resolve(action, attacker, defender);
    }

    private Action generateRandomAction()
    {
        List<Action> possibleActions = new LinkedList<>(Arrays.asList(Action.values()));
        //TODO: ADD % TO SURRENDER BASED ON LIFE OF THE MONSTER AND YOURS
        possibleActions.remove(Action.SURRENDER);
        //TODO: ADD VERIFICATION FOR HEALING, IF HE IS ABLE OR NOT TO DO IT
        return possibleActions.get(new Random().nextInt(possibleActions.size()));
    }

    private Action resolveAction(String keyboard)
    {
        Action action;
        switch (keyboard)
        {
            case "a":
            case "A":
                action = Action.ATTACK;
                break;
            case "u":
            case "U":
                action = Action.USE_POTION;
                break;
            case "s":
            case "S":
                action = Action.SURRENDER;
                break;
            default:
                action = Action.DO_NOTHING;
        }

        return action;
    }

    private boolean battleIsOver()
    {
        return getMonster().getHitPoints() < 0 || getHero().getHitPoints() < 0;
    }


}
