package rpg.battles;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.items.Item;
import rpg.livingbeings.Being;

import java.util.List;

@Getter
@Setter
@ToString
@Builder
public class BattleResult
{
    private double highestDamage;
    private Being winner;
    private List<Item> drops;
    private double damageDealtToMonster;
    private double damageSufferedFromMonster;
}
