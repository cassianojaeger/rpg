package rpg.battles;

import rpg.enums.Action;
import rpg.livingbeings.Being;
import rpg.livingbeings.heroes.Hero;
import rpg.livingbeings.monsters.Monster;

import java.util.Random;

public class ActionContext
{
    public RoundResult resolve(Action action, Being attacker, Being defender)
    {
        //todo: implement monster action strategy and hero action strategy
        // they will produce damage given, def increase or heal information

        RoundResult roundResult = RoundResult.builder()
                .actionPerformed(action)
                .attacker(attacker)
                .defender(defender)
                .hitPointsHealed(0.0)
                .damageDealt(0.0)
                .build();

        if (action.equals(Action.ATTACK))
        {
            Double weaponDamage = 0.0;
            Double penetration = 0.0;
            Double durability = 0.0;
            double attackerDamage = 0.0;

            if (attacker instanceof Hero)
            {
                weaponDamage = ((Hero) attacker).getWeapon().getDamage();
                penetration = ((Hero) attacker).getWeapon().getDefensePenetration();
                durability = ((Hero) attacker).getWeapon().getDurability();

                double baseDamage = ((weaponDamage + (weaponDamage * penetration)) * durability);
                attackerDamage = baseDamage + (baseDamage * (((new Random().nextDouble()) * 2.5) - 1));
            } else
            {
                attackerDamage = ((Monster) attacker).getDamage();
            }

            Double defenderHP = defender.getHitPoints();
            Double defenderDefense = defender.getDefense();

            double defenseReduction = (defenderDefense) / 1000;

            double damageDealt = attackerDamage - (attackerDamage * defenseReduction);

            roundResult.setDamageDealt(damageDealt);
            defender.setHitPoints(defenderHP - damageDealt);

            if (defender.getHitPoints() < 0)
            {
                roundResult.setBattleOver(true);
                roundResult.setWinner(attacker);
            }
        } else if (action.equals(Action.SURRENDER))
        {
            roundResult.setBattleOver(true);
            roundResult.setWinner(defender);
        }

        return roundResult;
    }
}
