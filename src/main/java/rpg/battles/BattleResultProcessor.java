package rpg.battles;

import rpg.livingbeings.Being;
import rpg.livingbeings.heroes.Hero;
import rpg.livingbeings.monsters.Monster;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

public class BattleResultProcessor
{

    public BattleResult processResults(List<RoundResult> results)
    {
        Supplier<DoubleStream> heroInflictedDamages = () -> results.stream()
                .filter(BattleResultProcessor::isHero)
                .mapToDouble(RoundResult::getDamageDealt);

        double inflictedDamage = heroInflictedDamages
                .get().sum();
        double highestDamageDealt = heroInflictedDamages
                .get().max().orElse(0.0);

        double totalDamageReceived = results.stream()
                .filter(BattleResultProcessor::isMonster)
                .mapToDouble(RoundResult::getDamageDealt)
                .sum();

        Being winner = results.stream()
                .filter(RoundResult::isBattleOver)
                .map(RoundResult::getWinner)
                .findFirst().orElse(null);

        return BattleResult.builder()
                .damageDealtToMonster(inflictedDamage)
                .damageSufferedFromMonster(totalDamageReceived)
                .highestDamage(highestDamageDealt)
                .drops(null).winner(winner).build();
    }

    private static boolean isMonster(RoundResult roundResult)
    {
        return roundResult.getAttacker() instanceof Monster;
    }

    private static boolean isHero(RoundResult round)
    {
        return round.getAttacker() instanceof Hero;
    }
}
