package rpg.battles;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import rpg.enums.Action;
import rpg.livingbeings.Being;

@Getter
@Setter
@ToString
@Builder
public class RoundResult
{
    private Being attacker;
    private Being defender;
    private Double damageDealt;
    private Double hitPointsHealed;
    private boolean battleOver;
    private Being Winner;
    private Action actionPerformed;
}
